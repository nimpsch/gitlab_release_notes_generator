from os import path

from setuptools import setup

# Get the long description from the README file
with open(path.join(path.dirname(__file__), "README.md")) as f:
    long_description = f.read()

with open("requirements.txt") as f:
    required = f.read().splitlines()

setup(
    name="gitlab-release-notes-generator",
    use_scm_version=True,  # This will generate the version number from git tags
    description="Utility for use in gitlab ci to generate changelogs.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/nimpsch/gitlab_release_notes_generator.git",
    author="Sebastian Nimpsch",
    author_email="snimpsch@gmail.com",
    license="MIT",
    py_modules=["gitlab_release_notes_generator"],
    setup_requires=["setuptools_scm"],
    install_requires=required,
    # To provide executable scripts, use entry points keyword. This should point to a function.
    # Entry points provide cross-platform support and allow
    # pip to create the appropriate form of executable for the target platform.
    entry_points={
        "console_scripts": [
            "gitlab_release_notes_generator=gitlab_release_notes_generator:main",
            "gitlab-release-notes-generator=gitlab_release_notes_generator:main",
        ],
    },
)
